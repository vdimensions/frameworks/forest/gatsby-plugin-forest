const fs = require("fs");
const path = require("path");

function resolvePageSource(p) {
    try {
       let result = require(p);
       if (result && result.render && (typeof result.render === `function`)) {
           return result.render();
       } else {
           return result;
       }
   } catch (e) {
       console.warn(`Could not require page from path '${p}', trying to parse as JSON`);
       return JSON.parse(fs.readFileSync(p));
   }
}

exports.loadForestPages = function(location) {
    return fs.readdirSync(location)
        .map(
            file => {
                const ext = path.extname(file).toLowerCase();
                if (ext === ".json" || ext === ".js") {
                    return [file.substring(0, file.length - path.extname(file).length), file];
                }
                return [];
            })
        .filter (x => x && x.length)
        .map(
            ([template, pageFile]) => {
                const pageSource = path.join(location, pageFile);
                return ({
                    template: template,
                    data: resolvePageSource(pageSource),
                    updatedAt: fs.statSync(pageSource).mtime
                });
            });
}

exports.node2ForestPage = function(node, componentPath) {
    const forestPage = JSON.parse(node.internal.content);
    return {
        path: `/${forestPage.template}`,
        component: componentPath,
        context: {
            ...forestPage,
            id: node.id
        },
    };
}

let warnOnceForManifestSupport = false;

exports.createNodeManifest = function({
        entryItem, // the raw data
        entryNode, // the Gatsby node
        createNodeManifest,
    }) {

    const isPreview = process.env.GATSBY_IS_PREVIEW === `true`;
    const createNodeManifestIsSupported = typeof createNodeManifest === `function`;
    const shouldCreateNodeManifest = isPreview && createNodeManifestIsSupported;

    if (shouldCreateNodeManifest) {
        const manifestId = `${entryItem.template}-${entryItem.updatedAt}`;
        createNodeManifest({
            manifestId,
            node: entryNode,
            updatedAtUTC: entryItem.updatedAt,
          });

    } else if (isPreview && !createNodeManifestIsSupported && !warnOnceForManifestSupport) {
        console.warn(
          `@vdimensions/gatsby-plugin-forest: Your version of Gatsby core doesn't support Content Sync (via the unstable_createNodeManifest action). Please upgrade to the latest version to use Content Sync in your site.`
        )
        // This is getting called for every entry node so we don't want the console logs to get cluttered
        warnOnceForManifestSupport = true;
    }
}